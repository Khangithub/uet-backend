const mongoose = require('mongoose');
const {formatClassname} = require('../../helpers/text');
const {uploadExcel} = require('../../helpers/firebase/uploadStorage');
const {
  generateFCreditsBaseOnGPA,
  randomFloat,
} = require('../../helpers/number');

const Class = require('../models/class');
const Student = require('../models/student');

exports.get_all_classes = (req, res, next) => {
  Class.find()
    .populate({
      path: 'consultant',
      select: 'fullname code profileImage academicRank',
    })
    .populate({
      path: 'faculty',
      select: 'code facultyName',
      populate: {
        path: 'dean',
        select: 'code',
      },
    })
    .then((docs) => {
      return res.status(200).json({docs});
    })
    .catch((error) => {
      res.status(500).json(error);
    });
};

exports.add_new_class = (req, res, next) => {
  const {data} = req;
  const {classname, consultant, schoolYear, faculty, startYear} = req.body;
  const {
    dropedOutListLength,
    notInteractListLength,
    promptedListLength,
    academicWarningListLength,
  } = req;

  const studentList = [];

  const newClass = new Class({
    _id: new mongoose.Types.ObjectId(),
    consultant,
    startYear,
    currentSchoolYear: schoolYear,
    currentSemester: 1,
    classname: formatClassname(classname),
    faculty,
    studentList,
    classSize: data.length,
    warnedLength:
      dropedOutListLength +
      notInteractListLength +
      promptedListLength +
      academicWarningListLength,
  });

  newClass
    .save()
    .then((doc) => {
      console.log('class added');
      return res.status(200).json({
        doc,
        message: 'Class added',
      });
    })
    .catch((error) => {
      console.log(error);
      res.status(500).json(error);
    });
};

exports.get_class_by_classname = (req, res, next) => {
  const {classname} = req.params;

  Class.find({classname})
    .then((_class) => {
      if (!_class) {
        return res.status(404).json({
          message: 'Class not found',
        });
      }

      res
        .status(200)
        .json({size: _class[0].studentList.length, _class: _class[0]});
    })
    .catch((error) => {
      res.status(500).json({
        error,
      });
    });
};

exports.edit_class_info = (req, res, next) => {
  const {classname} = req.params;
  const {newClassname, consultant, faculty} = req.body;

  Class.findOneAndUpdate(
    {classname},
    {classname: formatClassname(newClassname), consultant, faculty}
  )
    .exec()
    .then((doc) => {
      res.status(200).json({doc, message: 'Class updated'});
    })
    .catch((error) => {
      res.status(500).json({
        error,
      });
    });
};

exports.delete_class = (req, res, next) => {
  const {classname} = req.params;

  Class.deleteOne({classname})
    .then((doc) => res.status(200).json({doc, message: 'Class deleted'}))
    .catch((err) => res.status(500).json({err}));
};

exports.get_class_template = (req, res, next) => {
  const {classname} = req.params;

  Student.find({classname})
    .select('fullname code birthday')
    .exec()
    .then((docs) => {
      var studentInfoList = docs.map((student, index) => {
        const {code, fullname, birthday} = student;
        var item = {};

        item['STT '] = index + 1;
        item['Mã SV '] = code;
        item['Họ tên '] = fullname;
        item['Ngày sinh '] = birthday;
        item['Điểm trung bình trung tích lũy '] = randomFloat(0.5, 4.0);
        item['Điểm trung bình trung học kì '] = randomFloat(0.5, 4.0);
        item['Số tín chỉ còn thiếu '] = generateFCreditsBaseOnGPA({
          gpa: item['Điểm trung bình trung tích lũy '],
        });
        return item;
      });
      var getURL = uploadExcel(studentInfoList, `${classname}-template`);
      Promise.resolve(getURL)
        .then((downloadURL) => {
          return res.status(200).json({downloadURL});
        })
        .catch((error) => {
          console.log(err);
          return res.status(500).json({error});
        });
    })
    .catch((err) => {
      console.log(err);
      return res.status(500).json({err});
    });
};
