const mongoose = require('mongoose')
const Consultant = require('../models/consultant')

exports.get_all_consultants = (req, res, next) => {
  Consultant.find()
    .then(docs => {
      res.status(200).json({
        count: docs.length,
        docs
      })
    })
    .catch(error => {
      res.status(500).json(error)
    })
}

exports.create_new_consultant = (req, res, next) => {
  let {
    fullname,
    code,
    gender,
    birthday,
    email,
    address,
    academicRank,
    specialize
  } = req.body

  const consultant = new Consultant({
    _id: mongoose.Types.ObjectId(),
    fullname,
    code,
    gender,
    birthday,
    email,
    vnumail: code + '@vnu.edu.vn',
    profileImage: req.file.path,
    address,
    academicRank,
    specialize
  })

  consultant
    .save()
    .then(doc => {
      res.status(200).json({
        message: 'consultant stored',
        doc
      })
    })
    .catch(error => {
      console.log(error)
      res.status(500).json({ error })
    })
}

exports.get_consultant_by_id = (req, res, next) => {
  const { consultantId } = req.params

  Consultant.findById(consultantId)
    .then(doc => {
      if (!doc) {
        return res.status(404).json({
          message: 'Consultant not found'
        })
      }
      res.status(200).json({ doc })
    })
    .catch(error => {
      res.status(500).json({
        error
      })
    })
}

exports.edit_consultant_info = (req, res, next) => {
  const { consultantId } = req.params
  const { body } = req

  Consultant.findByIdAndUpdate(consultantId, body)
    .exec()
    .then(doc => {
      res.status(200).json({ consultantId, body, doc })
    })
    .catch(error => {
      res.status(500).json({
        error
      })
    })
}

exports.delete_consultant = (req, res, next) => {
  const { consultantId } = req.params

  Consultant.deleteOne({ _id: consultantId })
    .then(doc =>
      res.status(200).json({ message: 'consultant was deleted', doc })
    )
    .catch(error => res.status(500).json({ error }))
}
