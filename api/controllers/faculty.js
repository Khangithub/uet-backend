const Faculty = require('../models/faculty');
const mongoose = require('mongoose');

exports.get_all_faculties = (req, res, next) => {
  Faculty.find()
    .populate({
      path: 'dean',
      select: '_id fullname code profileImage academicRank',
    })
    .then((docs) => {
      return res.status(200).json({docs});
    })
    .catch((error) => {
      res.status(500).json(error);
    });
};

exports.add_new_faculty = (req, res, next) => {
  const {dean, code, facultyName, startYear} = req.body;

  const newFaculty = new Faculty({
    _id: new mongoose.Types.ObjectId(),
    dean,
    code,
    facultyName,
    startYear,
  });

  newFaculty
    .save()
    .then((doc) => {
      return res.status(200).json({
        message: 'Faculty added',
        doc,
      });
    })
    .catch((error) => {
      console.log(error);
      res.status(500).json(error);
    });
};

exports.edit_faculty_info = (req, res, next) => {
  const {code} = req.params;
  const {dean, newCode, facultyName, startYear} = req.body;

  Faculty.findOneAndUpdate({code}, {facultyName, code: newCode, dean, startYear})
    .exec()
    .then((doc) => {
      res.status(200).json({doc, message: 'Faculty updated'});
    })
    .catch((error) => {
      res.status(500).json({
        error,
      });
    });
};

exports.delete_faculty = (req, res, next) => {
  const {code} = req.params;

  Faculty.deleteOne({code})
    .exec()
    .then((doc) => {
      res.status(200).json({doc, message: 'Faculty deleted'});
    })
    .catch((error) => {
      console.log('error', error);
      res.status(500).json({
        error,
      });
    });
};
