const Parents = require('../models/parents');

exports.get_parents = (req, res, next) => {
  var {code} = req.params;

  Parents.findOne({
    code,
  })
    .exec()
    .then((doc) => {
      return res.status(200).json({
        doc,
      });
    })
    .catch((err) => {
      console.log(err);
      return res.status(500).json({
        err,
      });
    });
};

exports.get_current_parents = (req, res, next) => {
  const {currentUser} = req;

  Parents.findOne({
    code: currentUser.code,
  })
    .exec()
    .then((doc) => {
      return res.status(200).json({
        currentParents: doc,
      });
    })
    .catch((err) => {
      console.log(err);
      return res.status(500).json({
        err,
      });
    });
};

exports.edit_parents = (req, res, next) => {
  const {code} = req.params;
  const {
    dadAvatar,
    dadFullname,
    dadPhonenumber,
    dadJob,
    dadHometown,
    dadEmail,
    momAvatar,
    momFullname,
    momPhonenumber,
    momJob,
    momHometown,
    momEmail,
    parentPermanentAddress,
  } = req.body;

  if (
    /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im.test(
      dadPhonenumber
    ) === false
  ) {
    return res
      .status(400)
      .json({message: 'Phonenumber of Dad is invalid, reload page for more'});
  }

  if (
    /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im.test(
      momPhonenumber
    ) === false
  ) {
    return res
      .status(400)
      .json({message: 'Phonenumber of Mom is invalid, reload page for more'});
  }

  Parents.findOneAndUpdate(
    {
      code,
    },
    {
      $set: {
        dadAvatar,
        dadFullname,
        dadPhonenumber,
        dadJob,
        dadHometown,
        dadEmail,
        momAvatar,
        momFullname,
        momPhonenumber,
        momJob,
        momHometown,
        momEmail,
        parentPermanentAddress,
      },
    }
  )
    .exec()
    .then((doc) => {
      return res.status(200).json({
        doc,
        message: 'Parents updated, reload page for more',
      });
    })
    .catch((err) => {
      console.log(err);
      return res.status(500).json({
        err,
      });
    });
};
