const {classifyGPA} = require('../../helpers/number');
const {
  classifyAcademicWarningForFreshman,
  updateAcademicWarning,
} = require('../../helpers/object');

const Student = require('../models/student');

exports.get_students_from_class = (req, res, next) => {
  const {classname} = req.params;

  Student.find({classname})
    .populate({
      path: 'parentsMail',
      select:
        'code dadAvatar dadFullname dadEmail momAvatar momFullname momEmail',
    })
    .populate({
      path: 'consultant',
      select: 'fullname code profileImage academicRank email vnumail',
    })
    .exec()
    .then((docs) => {
      res.status(200).json({docs});
    })
    .catch((err) => {
      res.status(500).json({err});
    });
};

exports.get_student = (req, res, next) => {
  var {code} = req.params;

  Student.findOne({code})
    .exec()
    .then((doc) => {
      return res.status(200).json({doc});
    })
    .catch((error) => {
      console.log(error);
      return res.status(500).json({error});
    });
};

exports.update_student_gpa = (req, res, next) => {
  var {code} = req.params;

  var {gpa, schoolYear, semester} = req.body;

  Student.findOneAndUpdate(
    {
      code,
      scoreList: {
        $elemMatch: {
          semester,
          schoolYear,
        },
      },
    },
    {
      $set: {
        'scoreList.$.gpa': gpa,
        'scoreList.$.classification': classifyGPA(gpa),
      },
    }
  )
    .exec()
    .then((doc) => {
      console.log('gpa updated');
      next();
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({err});
    });
};

exports.update_student_gpa_semester = (req, res, next) => {
  var {code} = req.params;

  var {gpaSemester, schoolYear, semester} = req.body;

  Student.findOneAndUpdate(
    {
      code,
      scoreList: {
        $elemMatch: {
          semester,
          schoolYear,
        },
      },
    },
    {
      $set: {
        'scoreList.$.gpaSemester': gpaSemester,
      },
    }
  )
    .exec()
    .then((doc) => {
      console.log('gpaSemester updated');
      next();
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({err});
    });
};

exports.update_student_fCredits = (req, res, next) => {
  var {code} = req.params;
  var {fCredits, schoolYear, semester} = req.body;

  Student.findOneAndUpdate(
    {
      code,
      scoreList: {
        $elemMatch: {
          semester,
          schoolYear,
        },
      },
    },
    {
      $set: {
        'fCredits.$.credits': fCredits,
        'fCredits.$.schoolYear': schoolYear,
        'fCredits.$.semester': semester,
      },
    }
  )
    .exec()
    .then((doc) => {
      console.log('fCredits updated');
      next();
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({err});
    });
};

exports.re_evaluate_warnings = (req, res, next) => {
  var {code} = req.params;
  var {schoolYearIndex} = req;
  var {schoolYear, semester} = req.body;
  Student.findOne({
    code,
  })
    .select('fCredits scoreList')
    .exec()
    .then((doc) => {
      var {fCredits, scoreList} = doc;

      var {gpaSemester, gpa} = scoreList.filter((item, index) => {
        return item.schoolYear === schoolYear && item.semester === semester;
      })[0];

      var fCredit = fCredits.filter((item, index) => {
        return item.schoolYear === schoolYear && item.semester === semester;
      })[0];

      var credits = fCredit ? fCredit.credits : 0;

      var newWarning;
      if (schoolYearIndex === 0 && semester === '1') {
        newWarning = classifyAcademicWarningForFreshman(
          schoolYear,
          gpaSemester,
          gpa,
          credits
        );
      } else {
        newWarning = updateAcademicWarning(
          schoolYearIndex,
          schoolYear,
          semester,
          gpaSemester,
          gpa,
          credits
        );
      }

      Student.findOneAndUpdate(
        {
          code,
          scoreList: {
            $elemMatch: {
              semester,
              schoolYear,
            },
          },
        },
        {
          $set: {
            'warnings.$.reason': newWarning.reason,
            'warnings.$.schoolYear': newWarning.schoolYear,
            'warnings.$.semester': newWarning.semester,
          },
        }
      )
        .exec()
        .then((doc) => {
          console.log('new warnings updated');
          return res.status(200).json({doc});
        })
        .catch((err) => {
          console.log(err);
          res.status(500).json({err});
        });
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({err});
    });
};

exports.update_warning_manually = (req, res, next) => {
  var {code} = req.params;

  var {warnings, schoolYear, semester} = req.body;
  Student.findOneAndUpdate(
    {
      code,
      scoreList: {
        $elemMatch: {
          semester,
          schoolYear,
        },
      },
    },
    {
      $set: {
        'warnings.$.reason': warnings.reason,
        'warnings.$.schoolYear': warnings.schoolYear,
        'warnings.$.semester': warnings.semester,
      },
    }
  )
    .exec()
    .then((doc) => {
      console.log('manually new gpa updated');
      console.log(doc);
      return res.status(200).json({doc, message: 'manually new gpa updated'});
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({err});
    });
};

exports.delete_student = (req, res, next) => {
  var {code} = req.params;

  Student.deleteOne({code})
    .exec()
    .then((doc) => {
      console.log(`deleted student with code ${code} from colection Student`);
      next();
    })
    .catch((err) => {
      console.log(err);
      return res.status(500).json({err});
    });
};
