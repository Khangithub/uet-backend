require('dotenv').config();

const nodemailer = require('nodemailer');
const mongoose = require('mongoose');

const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const User = require('../models/user');

const {formatClassname} = require('../../helpers/text');
const {uploadExcel} = require('../../helpers/firebase/uploadStorage');

exports.get_all_none_student_users = (req, res, next) => {
  User.find({role: {$nin: ['student']}})
    // .select("email username role profileImage")
    .exec()
    .then((users) => {
      users
        ? res.status(200).json({counter: users.length, users})
        : res.status(400).json({message: 'user list empty'});
    })
    .catch((error) => res.status(400).json({error}));
};

exports.sign_up = (req, res, next) => {
  const {password, profileImage, code, role} = req.body;

  bcrypt.hash(password || code, 10, (err, hash) => {
    if (err) {
      return res.status(500).json({message: 'error in hash password', err});
    } else {
      const newUser = new User({
        _id: new mongoose.Types.ObjectId(),
        code,
        password: hash,
        role,
        profileImage,
        vnumail: code + '@vnu.edu.vn',
        activated: password && password !== code ? true : false,
      });

      newUser
        .save()
        .then((doc) => {
          req.createdUser = {doc, rawSignupPassword: req.body.password};
          next();
        })
        .catch((error) => {
          console.log(error);
          res.status(500).json(error);
        });
    }
  });
};

exports.login_as_staff = (req, res, next) => {
  var {vnumail, password} = req.body;

  if (vnumail === '') {
    return res.status(500).json({message: 'Emptied Mail'});
  }

  if (password === '') {
    return res.status(500).json({message: 'Emptied Password'});
  }

  if (
    /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/.test(
      vnumail
    ) === false
  ) {
    res.status(401).json({message: 'Invalid Mail'});
  }

  User.find({vnumail})
    .exec()
    .then((users) => {
      if (users.length < 1) {
        return res.status(401).json({message: 'Mail not found'});
      }

      bcrypt.compare(password, users[0].password, (err, isSamePassword) => {
        if (err) {
          console.log(err);
          return res.status(401).json({message: 'Verify error'});
        }

        if (!isSamePassword) {
          return res.status(401).json({message: 'Wrong password'});
        }

        const token = jwt.sign(
          {data: JSON.stringify(users[0]._id)},
          process.env.JWT_KEY,
          {
            expiresIn: '1d',
          }
        );

        return res.status(200).json({token, currentUser: users[0]});
      });
    })
    .catch((error) => {
      res.status(500).json({error});
    });
};

exports.login_as_student = (req, res, next) => {
  var {code, password} = req.body;

  if (code === '') {
    return res.status(500).json({message: 'Emptied Code'});
  }

  if (password === '') {
    return res.status(500).json({message: 'Emptied Password'});
  }

  if (/^[0-9]*$/gm.test(code) === false) {
    return res.status(500).json({message: 'Invalid Student Code'});
  }
  User.find({code})
    .exec()
    .then((users) => {
      if (users.length < 1) {
        return res.status(401).json({message: 'Code not found'});
      }

      bcrypt.compare(password, users[0].password, (err, isSamePassword) => {
        if (err) {
          console.log(err);
          return res.status(401).json({message: 'Verify error'});
        }

        if (!isSamePassword) {
          return res.status(401).json({message: 'Wrong password'});
        }

        const token = jwt.sign(
          {data: JSON.stringify(users[0]._id)},
          process.env.JWT_KEY,
          {
            expiresIn: '1d',
          }
        );

        return res.status(200).json({token, currentUser: users[0]});
      });
    })
    .catch((error) => {
      res.status(500).json({error});
    });
};

exports.get_user_by_id = (req, res, next) => {
  const {role, code} = req.params;

  User.findOne({role, code})
    .exec()
    .then((doc) => {
      return res.status(200).json({doc});
    })
    .catch((err) => {
      return res.status(500).json({err});
    });
};
exports.update_user = (req, res, next) => {
  const {userCode} = req.params;

  const {fullname, role, code, vnumail, academicRank, previousRole} = req.body;
  var hashedPassword = bcrypt.hashSync(code, 10);

  User.findOneAndUpdate(
    {code: userCode},
    {fullname, role, code, vnumail, academicRank, password: hashedPassword}
  )
    .exec()
    .then((doc) => {
      if (previousRole === 'consultant') {
        return next();
      } else {
        return res.status(200).json({doc, message: 'User updated'});
      }
    })
    .catch((error) => {
      res.status(500).json({
        error,
      });
    });
};
exports.delete_user = (req, res, next) => {
  const {userCode} = req.params;
  const {isConsultant} = req.body;

  User.deleteOne({code: userCode})
    .then((doc) => {
      if (isConsultant) {
        console.log('user from collection User was deleted');
        return next();
      } else {
        return res.status(200).json({
          doc,
          message: 'User deleted',
        });
      }
    })
    .catch((err) => res.status(500).json({err}));
};

exports.search = (req, res, next) => {
  var {keyword} = req.params;
  User.find()
    .or([
      {fullname: {$regex: keyword, $options: 'i'}},
      {code: {$regex: keyword, $options: 'i'}},
      {role: {$regex: keyword, $options: 'i'}},
    ])
    .exec()
    .then((docs) => {
      return res.status(200).json({count: docs.length, docs});
    })
    .catch((error) => res.status(500).json({error}));
};

exports.get_current_user = (req, res, next) => {
  var {currentUser} = req;
  return res.status(200).json({currentUser});
};

exports.edit_current_user = (req, res, next) => {
  var {body} = req;
  if (body.hasOwnProperty('newPass')) {
    var hashedPassword = bcrypt.hashSync(body.newPass, 10);

    User.findByIdAndUpdate(req.currentUser._id, {
      password: hashedPassword,
      activated: body.newPass === req.currentUser.code ? false : true,
    })
      .exec()
      .then((doc) => {
        return res.status(200).json({doc});
      })
      .catch((err) => res.status(500).json({err}));
  } else {
    User.findByIdAndUpdate(req.currentUser._id, body)
      .exec()
      .then((doc) => {
        return res.status(200).json({doc});
      })
      .catch((err) => res.status(500).json({err}));
  }
};

exports.export_xlsx = async (req, res, next) => {
  var {data, classname} = req.body;
  var filteredData = data.map((student, index) => {
    var {
      semester,
      classification,
      gpa,
      gpaSemester,
      schoolYear,
      fullname,
      gender,
      classname,
      birthday,
      code,
      fCredits,
      reason,
    } = student;

    var item = {};
    item['STT '] = index + 1;
    item['Họ tên '] = fullname;
    item['Mã SV '] = code;
    item['Ngày sinh '] = birthday;
    item['GT '] =
      (gender === 'No Record' && 'Chưa cập nhật') ||
      (gender === 'Male' && 'Nam') ||
      (gender === 'Female' && 'Nữ');
    item['Lớp '] = formatClassname(classname);
    item['Năm học '] = schoolYear;
    item['Học kì '] = semester;
    item['Lý do cảnh báo '] = reason === undefined ? '' : reason.join(' \n');
    item['Điểm trung bình trung tích lũy '] = gpa;
    item['Điểm trung bình trung học kì '] = gpaSemester;
    item['Phân loại điểm trung bình trung tích lũy '] = classification;
    item['Số tín chỉ còn thiếu '] = fCredits;

    return item;
  });

  var getURL = uploadExcel(filteredData, classname);
  Promise.resolve(getURL)
    .then((downloadURL) => {
      return res.status(200).json({downloadURL});
    })
    .catch((error) => {
      return res.status(500).json({error});
    });
};

exports.send_mail = (req, res, next) => {
  var {subjectTo, mailList, content} = req.body;
  var {attachURL} = req;

  var transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
      user: process.env.EMAIL,
      pass: process.env.PASSWORD,
    },
  });

  var mailOptions = {
    from: 'sale.shopeeholic@gmail.com',
    to: mailList,
    cc: mailList,
    subject: subjectTo,
    text: `${content} \n Attached files: ${attachURL}`,
  };

  transporter.sendMail(mailOptions, (err, info) => {
    if (err) {
      console.log(err);
      return res.status(400).json({err});
    } else {
      return res.status(200).json({message: `Mail sent to ${mailList}`});
    }
  });
};

exports.merge_mail = (req, res, next) => {
  console.log('merge mail begin');
  const mailOptionList = req.body;
  var counter = 0;

  var transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
      user: process.env.EMAIL,
      pass: process.env.PASSWORD,
    },
  });

  for (var i = 0; i < mailOptionList.length; i++) {
    var mailOptions = {
      from: 'sale.shopeeholic@gmail.com',
      to: mailOptionList[i].mails.join(','),
      cc: mailOptionList[i].mails.join(','),
      subject: mailOptionList[i].mailTitle,
      text: mailOptionList[i].mailContent,
    };

    transporter.sendMail(mailOptions, (err, info) => {
      if (err) {
        console.log(err);
        return res
          .status(400)
          .json({err, message: `trouble in sending mail at index ${i}`});
      } else {
        console.log(`mail sent to ${JSON.stringify(mailOptionList[i].mails)}`);
        counter++;
      }
    });
    console.log(`mail sent to ${JSON.stringify(mailOptionList[i].mails)}`);
    counter++;
    console.log(counter);
  }

  if (counter === mailOptionList.length) {
    return res.status(200).json({message: 'mail sent all'});
  }
};

exports.add_new_user_to_collection_User = (req, res, next) => {
  const {role, fullname, code, academicRank} = req.body;

  const newUser = new User({
    _id: new mongoose.Types.ObjectId(),
    role,
    fullname,
    code,
    vnumail: code.trim() + '@vnu.edu.vn',
    email: code.trim() + '@gmail.com',
    academicRank,
    ...req.newUserData,
  });

  newUser
    .save()
    .then((doc) => {
      console.log('New User added to collection User');
      return res.status(200).json({
        message: 'User added',
        doc,
      });
    })
    .catch((error) => {
      console.log(error);
      res.status(500).json(error);
    });
};
