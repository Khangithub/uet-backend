const xlsx = require('xlsx');
const Class = require('../models/class');
const {AvatarGenerator} = require('random-avatar-generator');

const {formatClassname} = require('../../helpers/text');
const {classifyGPA} = require('../../helpers/number');
const {
  classifyAcademicWarningForFreshman,
  updateAcademicWarning,
} = require('../../helpers/object');
const Student = require('../models/student');

exports.check_class = (req, res, next) => {
  const {classname} = req.params;

  Class.find({classname})
    .exec()
    .then((doc) => {
      if (doc.length >= 1) {
        res.status(400).json({message: 'class existed'});
      } else {
        next();
      }
    })
    .catch((err) => {
      console.log('error in finding class');
      res.status(500).json({err});
    });
};

exports.get_schoolYear_index = (req, res, next) => {
  var {classname, schoolYear} = req.body;

  Class.find({classname})
    .distinct('startYear')
    .exec()
    .then((docs) => {
      var startYear = parseInt(docs[0]);
      var schoolYearIndex = parseInt(schoolYear) - startYear;

      if (schoolYearIndex < 0) {
        return res.status(400).json({
          message: `The current school year (${schoolYear}) is less than the school year it started (${startYear})`,
        });
      }

      req.schoolYearIndex = schoolYearIndex;
      next();
    })
    .catch((err) => {
      console.log(err);
      return res.status(500).json({err});
    });
};

exports.extract_data = (req, res, next) => {
  const generator = new AvatarGenerator();
  let {file, schoolYearIndex} = req;
  let workbook = xlsx.readFile(file.path);
  const sheet_name_list = workbook.SheetNames;
  let {
    classname,
    schoolYear,
    semester,
    startYear,
    isStartYear,
    faculty,
  } = req.body;

  var data = [];

  sheet_name_list.forEach((sheet) => {
    let workSheet = workbook.Sheets[sheet];

    let dataArr = xlsx.utils.sheet_to_json(workSheet);
    dataArr.forEach((info) => {
      var fullname = info['Họ tên '];
      var code = info['Mã SV '];
      var birthday = info['Ngày sinh '];
      var gpaSemester = info['Điểm trung bình trung học kì '];
      var gpa = info['Điểm trung bình trung tích lũy '];
      var shortenCredits = info['Số tín chỉ còn thiếu '];

      data.push({
        fullname,
        code,
        birthday,
        startYear,
        profileImage: generator.generateRandomAvatar(),
        faculty,
        classname: formatClassname(classname),
        fCredits: {
          credits: shortenCredits,
          schoolYear,
          semester,
        },
        scoreList: {
          gpa,
          gpaSemester,
          schoolYear,
          semester,
          classification: classifyGPA(gpa),
        },
        warnings:
          semester === '1' && isStartYear === 'true'
            ? classifyAcademicWarningForFreshman(
                schoolYear,
                gpaSemester,
                gpa,
                shortenCredits
              )
            : updateAcademicWarning(
                schoolYearIndex,
                schoolYear,
                semester,
                gpaSemester,
                gpa,
                shortenCredits
              ),
      });
    });
  });

  console.log(data);
  req.data = data;
  next();
};

exports.update_class = (req, res, next) => {
  const {classname} = req.params;
  const {schoolYear, semester} = req.body;
  const {
    dropedOutListLength,
    notInteractListLength,
    promptedListLength,
    academicWarningListLength,
  } = req;
  console.log(req.body);
  Class.findOneAndUpdate(
    {classname},
    {
      currentSchoolYear: schoolYear,
      currentSemester: semester,
      warnedLength:
        dropedOutListLength +
        notInteractListLength +
        promptedListLength +
        academicWarningListLength,
    }
  )
    .exec()
    .then((doc) => {
      res.status(200).json({doc, message: 'Class updated'});
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({
        err,
      });
    });
};

exports.decrease_current_classSize_and_warnedLength_by_one = (
  req,
  res,
  next
) => {
  const {classname, code} = req.params;
  const {currentClassSize, currentWarnedLength} = req;

  console.log(currentClassSize, currentWarnedLength);

  Class.findOneAndUpdate({classname}, {$inc: {classSize: -1, warnedLength: -1}})
    .exec()
    .then((doc) => {
      return res.status(200).json({
        message: `student with code ${code} was deleted from collections Parents, Users, Students`,
        doc,
      });
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({
        err,
      });
    });
};
