const Consultant = require('../models/consultant');

exports.delete_consultant = (req, res, next) => {
  const {userCode} = req.params;

  Consultant.deleteOne({code: userCode})
    .then((doc) => {
      console.log('user from collection Consultant was deleted');
      return res.status(200).json({
        doc,
        message: 'User deleted from collection Consultant and User',
      });
    })
    .catch((err) => res.status(500).json({err}));
};

exports.update_consultant = (req, res, next) => {
  const {userCode} = req.params;

  const {fullname, role, code, vnumail, academicRank} = req.body;
  console.log('updated user from collection User');
  Consultant.findOneAndUpdate(
    {code: userCode},
    {fullname, role, code, vnumail, academicRank}
  )
    .exec()
    .then((doc) => {
      console.log('updated user from collection Consultant');
      return res.status(200).json({
        doc,
        message: 'User was updated in both collection User and Consultant',
      });
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({
        err,
      });
    });
};
