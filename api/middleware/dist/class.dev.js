"use strict";

var xlsx = require('xlsx');

var Class = require('../models/class');

var _require = require('random-avatar-generator'),
    AvatarGenerator = _require.AvatarGenerator;

var _require2 = require('../../helpers/text'),
    formatClassname = _require2.formatClassname;

var _require3 = require('../../helpers/number'),
    classifyGPA = _require3.classifyGPA;

var _require4 = require('../../helpers/object'),
    classifyAcademicWarningForFreshman = _require4.classifyAcademicWarningForFreshman,
    updateAcademicWarning = _require4.updateAcademicWarning;

var Student = require('../models/student');

exports.check_class = function (req, res, next) {
  var classname = req.params.classname;
  Class.find({
    classname: classname
  }).exec().then(function (doc) {
    if (doc.length >= 1) {
      res.status(400).json({
        message: 'class existed'
      });
    } else {
      next();
    }
  })["catch"](function (err) {
    console.log('error in finding class');
    res.status(500).json({
      err: err
    });
  });
};

exports.get_schoolYear_index = function (req, res, next) {
  var _req$body = req.body,
      classname = _req$body.classname,
      schoolYear = _req$body.schoolYear;
  Class.find({
    classname: classname
  }).distinct('startYear').exec().then(function (docs) {
    var startYear = parseInt(docs[0]);
    var schoolYearIndex = parseInt(schoolYear) - startYear;

    if (schoolYearIndex < 0) {
      return res.status(400).json({
        message: "The current school year (".concat(schoolYear, ") is less than the school year it started (").concat(startYear, ")")
      });
    }

    req.schoolYearIndex = schoolYearIndex;
    next();
  })["catch"](function (err) {
    console.log(err);
    return res.status(500).json({
      err: err
    });
  });
};

exports.extract_data = function (req, res, next) {
  var generator = new AvatarGenerator();
  var file = req.file,
      schoolYearIndex = req.schoolYearIndex;
  var workbook = xlsx.readFile(file.path);
  var sheet_name_list = workbook.SheetNames;
  var _req$body2 = req.body,
      classname = _req$body2.classname,
      schoolYear = _req$body2.schoolYear,
      semester = _req$body2.semester,
      startYear = _req$body2.startYear,
      isStartYear = _req$body2.isStartYear,
      faculty = _req$body2.faculty;
  var data = [];
  sheet_name_list.forEach(function (sheet) {
    var workSheet = workbook.Sheets[sheet];
    var dataArr = xlsx.utils.sheet_to_json(workSheet);
    dataArr.forEach(function (info) {
      var fullname = info['Họ tên '];
      var code = info['Mã SV '];
      var birthday = info['Ngày sinh '];
      var gpaSemester = info['Điểm trung bình trung học kì '];
      var gpa = info['Điểm trung bình trung tích lũy '];
      var shortenCredits = info['Số tín chỉ còn thiếu '];
      data.push({
        fullname: fullname,
        code: code,
        birthday: birthday,
        startYear: startYear,
        profileImage: generator.generateRandomAvatar(),
        faculty: faculty,
        classname: formatClassname(classname),
        fCredits: {
          credits: shortenCredits,
          schoolYear: schoolYear,
          semester: semester
        },
        scoreList: {
          gpa: gpa,
          gpaSemester: gpaSemester,
          schoolYear: schoolYear,
          semester: semester,
          classification: classifyGPA(gpa)
        },
        warnings: semester === '1' && isStartYear === 'true' ? classifyAcademicWarningForFreshman(schoolYear, gpaSemester, gpa, shortenCredits) : updateAcademicWarning(schoolYearIndex, schoolYear, semester, gpaSemester, gpa, shortenCredits)
      });
    });
  });
  console.log(data);
  req.data = data;
  next();
};

exports.update_class = function (req, res, next) {
  var classname = req.params.classname;
  var _req$body3 = req.body,
      schoolYear = _req$body3.schoolYear,
      semester = _req$body3.semester;
  var dropedOutListLength = req.dropedOutListLength,
      notInteractListLength = req.notInteractListLength,
      promptedListLength = req.promptedListLength,
      academicWarningListLength = req.academicWarningListLength;
  console.log(req.body);
  Class.findOneAndUpdate({
    classname: classname
  }, {
    currentSchoolYear: schoolYear,
    currentSemester: semester,
    warnedLength: dropedOutListLength + notInteractListLength + promptedListLength + academicWarningListLength
  }).exec().then(function (doc) {
    res.status(200).json({
      doc: doc,
      message: 'Class updated'
    });
  })["catch"](function (err) {
    console.log(err);
    res.status(500).json({
      err: err
    });
  });
};

exports.decrease_current_classSize_and_warnedLength_by_one = function (req, res, next) {
  var _req$params = req.params,
      classname = _req$params.classname,
      code = _req$params.code;
  var currentClassSize = req.currentClassSize,
      currentWarnedLength = req.currentWarnedLength;
  console.log(currentClassSize, currentWarnedLength);
  Class.findOneAndUpdate({
    classname: classname
  }, {
    $inc: {
      classSize: -1,
      warnedLength: -1
    }
  }).exec().then(function (doc) {
    return res.status(200).json({
      message: "student with code ".concat(code, " was deleted from collections Parents, Users, Students"),
      doc: doc
    });
  })["catch"](function (err) {
    console.log(err);
    res.status(500).json({
      err: err
    });
  });
};