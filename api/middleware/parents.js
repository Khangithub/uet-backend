const Parents = require('../models/parents');
const {AvatarGenerator} = require('random-avatar-generator');
const faker = require('faker');

exports.add_parent_from_excel = (req, res, next) => {
  const {data} = req;
  const generator = new AvatarGenerator();
  var parentList = [];

  data.forEach((parents, index) => {
    var {code, faculty, classname} = parents;
    var dadFirstName = faker.name.firstName();
    var dadLastName = faker.name.lastName();

    var momFirstName = faker.name.firstName();
    var momLastName = faker.name.lastName();

    parentList.push({
      code,
      faculty,
      classname,
      dadAvatar: generator.generateRandomAvatar(),
      dadFullname: dadFirstName + ' ' + dadLastName,
      dadPhonenumber: faker.phone.phoneNumber(),
      dadJob: faker.name.jobTitle(),
      dadHometown: faker.address.city() + '/' + faker.address.country(),
      dadEmail:
        dadFirstName.toLowerCase() +
        '.' +
        dadLastName.toLowerCase() +
        '@gmail.com',

      momAvatar: generator.generateRandomAvatar(),
      momFullname: momFirstName + ' ' + momLastName,
      momPhonenumber: faker.phone.phoneNumber(),
      momJob: faker.name.jobTitle(),
      momHometown: faker.address.city() + '/' + faker.address.country(),
      momEmail:
        momFirstName.toLowerCase() +
        '.' +
        momLastName.toLowerCase() +
        '@gmail.com',

      parentPermanentAddress:
        faker.address.city() + '/' + faker.address.country(),
    });
  });

  Parents.insertMany(parentList)
    .then((docs) => {
      console.log('new parentses were inserted, reload the database');
      next();
    })
    .catch((err) => {
      if (
        (err.name === 'BulkWriteError' || err.name === 'MongoError') &&
        err.code === 11000
      ) {
        console.log('new parentses were inserted, reload the database');
        next();
      } else {
        res.status(500).json({err});
      }
    });
};

exports.delete_parents = (req, res, next) => {
  const {classname} = req.params;

  Parents.deleteMany({
    classname,
  })
    .then((doc) => {
      console.log(`Parents from class ${classname} were deleted`);
      next();
    })
    .catch((err) => {
      res.status(500).json({err});
    });
};

exports.delele_student_from_parents = (req, res, next) => {
  var {code} = req.params;

  Parents.deleteOne({code})
    .exec()
    .then((doc) => {
      console.log(`deleted parents with code ${code} from colection Parents`);
      next();
    })
    .catch((err) => {
      console.log(err);
      return res.status(500).json({err});
    });
};
