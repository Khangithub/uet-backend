const Student = require('../models/student');
const {formatClassname} = require('../../helpers/text');

exports.check_semester = (req, res, next) => {
  var {schoolYear, semester, classname} = req.body;
  var classname = formatClassname(classname);
  Student.find({
    classname,
    accademicTrainningList: {
      $elemMatch: {
        semester,
        schoolYear,
      },
    },
  })
    .exec()
    .then((docs) => {
      if (docs.length === 0) {
        next();
      } else {
        console.log(docs);
        res.status(400).json({
          message: `some students of class ${classname} has academicTrainningScore at semester ${semester} and schoolYear ${schoolYear} existed`,
        });
      }
    })
    .catch((err) => {
      console.log('problem in finding schoolYear and semester');
      res.status(500).json({err});
    });
};

exports.add_students_from_excel = async (req, res, next) => {
  const {data} = req;
  const {consultant} = req.body;

  var studentList = [];

  data.forEach((student, index) => {
    var {
      fullname,
      code,
      birthday,
      classname,
      profileImage,
      faculty,
      startYear,
      fCredits,
      scoreList,
      warnings,
    } = student;

    studentList.push({
      consultant,
      fullname,
      birthday,
      classname,
      faculty,
      startYear,
      profileImage,
      code,
      vnumail: code + '@vnu.edu.vn',
      classname,
      fCredits,
      scoreList,
      warnings,
    });
  });

  Student.insertMany(studentList)
    .then((docs) => {
      console.log('new students were inserted, reload the database');
      next();
    })
    .catch((err) => {
      if (
        (err.name === 'BulkWriteError' || err.name === 'MongoError') &&
        err.code === 11000
      ) {
        console.log('new students were inserted, reload the database');
        next();
      } else {
        console.log('error in insert sudent', err);
        res.status(500).json({err});
      }
    });
};

exports.delete_students_from_class = (req, res, next) => {
  const {classname} = req.params;

  Student.deleteMany({
    classname,
  })
    .then((doc) => {
      console.log(`students from class ${classname} were deleted`);
      next();
    })
    .catch((err) => {
      res.status(500).json({err});
    });
};

exports.update_students = (req, res, next) => {
  var {classname, semester, schoolYear} = req.body;

  const {data} = req;

  const size = data.length;
  var counter = 0;

  data.forEach((student) => {
    const {code, warnings, scoreList, fCredits} = student;

    Student.findOneAndUpdate(
      {classname, code},
      {$push: {warnings, scoreList, fCredits}},
      {upsert: true},
      (err, result) => {
        // console.log(result)
        if (err) {
          res.status(500).json({err});
        }
        counter++;

        if (counter === size) {
          console.log('new core list was updated');
          next();
        }
      }
    );
  });
};

exports.count_droped_out_students = (req, res, next) => {
  var {data} = req;
  var dropedOutList = data.filter((student) => {
    return student.warnings.reason.length === 3;
  });

  console.log('dropedOutList', dropedOutList.length);
  req.dropedOutListLength = dropedOutList.length;
  next();
};

exports.count_not_interact_students = (req, res, next) => {
  var {data} = req;
  var notInteractList = data.filter((student) => {
    return student.scoreList.gpa === 0;
  });

  console.log('notInteractList', notInteractList.length);
  req.notInteractListLength = notInteractList.length;
  next();
};

exports.count_prompted_students = (req, res, next) => {
  var {data} = req;
  var promptedList = data.filter((student) => {
    return student.scoreList.gpa < 1 || student.fCredits.credits > 24;
  });

  console.log('promptedList', promptedList.length);
  req.promptedListLength = promptedList.length;
  next();
};

exports.count_subject_to_academic_warning_students = (req, res, next) => {
  var {data} = req;
  var academicWarningList = data.filter((student) => {
    return student.scoreList.gpa < 1.2;
  });

  console.log('academicWarningList', academicWarningList.length);
  req.academicWarningListLength = academicWarningList.length;
  next();
};
