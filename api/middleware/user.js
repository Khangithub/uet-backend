const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const {uploadFile} = require('../../helpers/firebase/uploadStorage');
const User = require('../models/user');
const Consultant = require('../models/consultant');
const UetLeader = require('../models/uetLeader');
const ObjectId = require('mongodb').ObjectID;
const {AvatarGenerator} = require('random-avatar-generator');
const mongoose = require('mongoose');

exports.auth = (req, res, next) => {
  let token = req.headers.authorization.split(' ')[1];
  let decoded = jwt.verify(token, process.env.JWT_KEY);
  let myId = JSON.parse(decoded.data);

  User.findOne({_id: ObjectId(myId)})
    .select('-password -__v')
    .exec()
    .then((user) => {
      if (user) {
        req.currentUser = user;
        next();
      } else {
        res.status(400).json({
          message: 'user not found',
        });
      }
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({
        err,
      });
    });
};

exports.is_specialist = (req, res, next) => {
  return req.currentUser.role === 'specialist'
    ? next()
    : res.status(500).json({message: 'you are not specialist'});
};

exports.send_mail_confirm = (req, res, next) => {
  const {createdUser} = req;
  return res.status(200).json({
    createdUser,
  });
};

exports.add_user_from_excel = (req, res, next) => {
  const {data} = req;
  var studentList = data.map((student) => {
    const {fullname, code, profileImage, classname} = student;
    var hashedPassword = bcrypt.hashSync(code.toString(), 10);

    return {
      fullname,
      profileImage,
      code,
      classname,
      vnumail: code + '@vnu.edu.vn',
      password: hashedPassword,
      activated: false,
    };
  });

  User.insertMany(studentList, {ordered: false})
    .then((docs) => {
      console.log('new users were inserted, reload the database');
      next();
    })
    .catch((err) => {
      if (
        (err.name === 'BulkWriteError' || err.name === 'MongoError') &&
        err.code === 11000
      ) {
        console.log('new users were inserted, reload the database');
        next();
      } else {
        console.log('error in insertMany user from excel');
        res.status(500).json({err});
      }
    });
};

exports.delete_students_from_user = (req, res, next) => {
  const {classname} = req.params;

  User.deleteMany({
    classname,
  })
    .then((doc) => {
      console.log(`deleted user in class ${classname}`);
      next();
    })
    .catch((err) => {
      res.status(500).json({err});
    });
};

exports.upload_cloud_attach = (req, res, next) => {
  var {file} = req;
  if (file) {
    var getURL = uploadFile(file);
    Promise.resolve(getURL)
      .then((attachURL) => {
        console.log(attachURL);
        req.attachURL = attachURL;
        next();
      })
      .catch((error) => {
        return res.status(500).json({error});
      });
  } else {
    next();
  }
};

exports.delele_student_from_user = async (req, res, next) => {
  var {code} = req.params;

  User.deleteOne({code})
    .then((doc) => {
      console.log(`deleted user with code ${code} from collection Users`);
      next();
    })
    .catch((err) => res.status(500).json({err}));
};

exports.add_new_user_to_specific_collection = (req, res, next) => {
  const {role, fullname, code, academicRank} = req.body;

  const generator = new AvatarGenerator();
  const hashedPassword = bcrypt.hashSync(code.trim().toString(), 10);
  console.log(role);
  const newUserData = {
    password: hashedPassword,
    profileImage: generator.generateRandomAvatar(),
  };

  if (role === 'consultant') {
    var newConsultant = new Consultant({
      _id: new mongoose.Types.ObjectId(),
      fullname,
      code,
      vnumail: code.trim() + '@vnu.edu.vn',
      email: code.trim() + '@gmail.com',
      academicRank,
      ...newUserData,
    });

    newConsultant
      .save()
      .then((doc) => {
        console.log('New User as Consultant was added');
        req.newUserData = newUserData;
        next();
      })
      .catch((err) => {
        res.status(500).json({err});
      });
  } else {
    req.newUserData = newUserData;
    next();
  }
};
