const mongoose = require('mongoose');

const classSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  consultant: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Consultant',
    required: true,
  },
  faculty: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Faculty',
    required: true,
  },
  startYear: {
    type: String,
    required: true,
  },
  currentSchoolYear: {
    type: String,
    required: true,
  },
  currentSemester: {
    type: String,
    required: true,
  },
  classname: {
    type: String,
    required: true,
    unique: true,
  },
  classSize: {
    type: Number,
    required: true,
  },
  warnedLength: {
    type: Number,
    required: true,
  },
});

const Class = mongoose.model('Class', classSchema);

module.exports = Class;
