const mongoose = require('mongoose');

const consultantSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  fullname: {
    type: String,
    required: true,
  },
  code: {
    type: String,
    required: true,
  },
  gender: {
    type: String,
    required: true,
    enum: ['Male', 'Female', 'No Record'],
    default: 'No Record',
  },
  birthday: {
    type: Date,
  },
  email: {
    type: String,
  },
  vnumail: {
    type: String,
  },
  profileImage: {
    type: String,
  },
  address: {
    type: String,
  },
  academicRank: {
    type: String,
    required: true,
    enum: ['master', 'phd', 'professor'],
    default: 'master',
  },
  specialize: {
    type: String,
  },
});

const Consultant = mongoose.model('Consultant', consultantSchema);

module.exports = Consultant;

