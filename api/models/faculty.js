const mongoose = require('mongoose');

const facultySchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  dean: {
    // trưởng khoa
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true,
    unique: true
  },
  code: {
    type: String,
    required: true,
    unique: true,
  },
  facultyName: {
    type: String,
    required: true,
    unique: true,
  },
  startYear: {
    type: String,
  },
});

const Faculty = mongoose.model('Faculty', facultySchema);

module.exports = Faculty;
