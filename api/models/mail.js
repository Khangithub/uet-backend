const mongoose = require('mongoose')

const mailSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  sender: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  receiver: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  reasons: [
    {
      content: {
        type: String,
        required: true
      }
    }
  ],
  attachs: [
    {
      link: {
        type: String
      }
    }
  ]
})

const Mail = mongoose.model('Mail', mailSchema)

module.exports = Mail
