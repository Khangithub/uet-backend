const mongoose = require('mongoose');

const parentsSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,

  code: {
    type: String,
    required: true,
    unique: true,
  },
  classname: {
    type: String,
    required: true,
  },
  dadAvatar: {
    type: String,
  },
  dadFullname: {
    type: String,
  },
  dadPhonenumber: {
    type: String,
    unique: true,
  },
  dadJob: {
    type: String,
  },
  faculty: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Faculty',
    required: true,
  },
  dadEmail: {
    type: String,
    required: true,
    unique: true,
    match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/,
  },
  dadHometown: {
    type: String,
  },
  momAvatar: {
    type: String,
  },
  momFullname: {
    type: String,
  },
  momPhonenumber: {
    type: String,
    unique: true,
  },
  momJob: {
    type: String,
  },
  momEmail: {
    type: String,
    required: true,
    unique: true,
    match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/,
  },
  momHometown: {
    type: String,
  },
  parentPermanentAddress: {
    type: String,
  },
});

const Parents = mongoose.model('Parents', parentsSchema);

module.exports = Parents;
