const mongoose = require('mongoose');

const studentSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  fullname: {
    type: String,
  },
  code: {
    type: String,
    required: true,
    unique: true,
  },
  faculty: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Faculty',
    required: true,
  },
  consultant: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Consultant',
    required: true,
  },
  startYear: {
    type: String,
    required: true,
  },
  classname: {
    type: String,
    require: true,
  },
  gender: {
    type: String,
    required: true,
    enum: ['Male', 'Female', 'No Record'],
    default: 'No Record',
  },
  birthday: {
    type: String,
  },
  vnumail: {
    type: String,
    unique: true,
    required: true,
    match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/,
  },
  vnumail: {
    type: String,
    match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/,
  },
  profileImage: {
    type: String,
    default:
      'https://kittyinpink.co.uk/wp-content/uploads/2016/12/facebook-default-photo-male_1-1.jpg',
  },
  hometown: {
    type: String,
  },
  warnings: [
    {
      schoolYear: {
        type: String,
      },
      semester: {
        type: String,
        enum: ['1', '2'],
        default: '1',
      },
      reason: {
        type: Array,
      },
      mail: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Mail',
      },
    },
  ],
  fCredits: [
    {
      credits: {
        type: Number,
        required: true,
      },
      schoolYear: {
        type: String,
        required: true,
      },
      semester: {
        type: String,
        required: true,
        enum: ['1', '2'],
        default: '1',
      },
    },
  ],
  scoreList: [
    {
      gpa: {
        type: Number,
        required: true,
      },
      gpaSemester: {
        type: Number,
        required: true,
      },
      schoolYear: {
        type: String,
        required: true,
      },
      semester: {
        type: String,
        required: true,
        enum: ['1', '2'],
        default: '1',
      },
      classification: {
        type: String,
        required: true,
        enum: [
          'Aplus',
          'Ascore',
          'Bplus',
          'Bscore',
          'Cscore',
          'Dscore',
          'Fscore',
        ],
        default: 'No Record',
      },
    },
  ],
  receiveScholarship: [
    {
      scholarshipName: {
        type: String,
        required: true,
      },
      value: {
        type: Number,
        required: true,
      },
    },
  ],
  prizeList: [
    {
      constestName: {
        type: String,
        required: true,
      },
      ranking: {
        type: Number,
        required: true,
      },
    },
  ],
  scienceContestPrizeList: [
    {
      constestName: {
        type: String,
        required: true,
      },
      ranking: {
        type: Number,
        required: true,
      },
    },
  ],
  wentAbroad: [
    {
      country: {
        type: String,
      },
      time: {
        type: Date,
      },
    },
  ],
  tookTheTest: [
    {
      testName: {
        type: String,
        required: true,
      },
      ranking: {
        type: Number,
        required: true,
      },
    },
  ],
  punishList: [
    {
      studentCode: {
        type: mongoose.Schema.Types.ObjectId,
      },
    },
  ],
});

studentSchema.virtual('parentsMail', {
  ref: 'Parents',
  localField: 'code',
  foreignField: 'code',
  justOne: true,
});

studentSchema.set('toObject', {virtuals: true});
studentSchema.set('toJSON', {virtuals: true});

const Student = mongoose.model('Students', studentSchema);

module.exports = Student;
