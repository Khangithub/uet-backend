const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  code: {
    type: String,
    required: true,
    unique: true,
  },
  classname: {
    type: String,
  },
  fullname: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  activated: {
    type: Boolean,
  },
  vnumail: {
    type: String,
    required: true,
    unique: true,
    match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/,
  },
  profileImage: {
    type: String,
    default:
      'https://kittyinpink.co.uk/wp-content/uploads/2016/12/facebook-default-photo-male_1-1.jpg',
  },

  role: {
    type: String,
    required: true,
    enum: [
      'consultant',
      'parent',
      'student',
      'specialist',
      'uet-leader',
      'student-affair-leader',
      'academic-leader',
    ],
    default: 'student',
  },
   academicRank: {
    type: String,
    required: true,
    enum: ['master', 'phd', 'professor'],
    default: 'master',
  },
});

const User = mongoose.model('User', userSchema);

module.exports = User;
