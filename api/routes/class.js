const express = require('express');
const {upload} = require('../../helpers/multer');
const router = express.Router();

const {
  get_all_classes,
  add_new_class,
  get_class_by_classname,
  edit_class_info,
  delete_class,
  get_class_template,
} = require('../controllers/class');

const {
  check_semester,
  add_students_from_excel,
  delete_students_from_class,
  update_students,
  count_droped_out_students,
  count_not_interact_students,
  count_prompted_students,
  count_subject_to_academic_warning_students,
  
} = require('../middleware/student');

const {
  add_parent_from_excel,
  delete_parents,
} = require('../middleware/parents');

const {
  add_user_from_excel,
  delete_students_from_user,
  auth,
  is_specialist,
} = require('../middleware/user');

const {
  extract_data,
  check_class,
  get_schoolYear_index,
  update_class,
} = require('../middleware/class');

router.get('', get_all_classes);

// {classname: K61-CA?clc2!,
//     excel: k1cajk.xlsx,
//     schoolYear: 2016,
//     consultant: 5f4ccd118cf23c4938b73b9e
// }

router.post(
  '/',
  check_class,
  upload.single('excel'),
  extract_data,
  add_students_from_excel,
  add_parent_from_excel,
  add_user_from_excel,
  count_droped_out_students,
  count_not_interact_students,
  count_prompted_students,
  count_subject_to_academic_warning_students,
  add_new_class
);

router.post(
  '/:classname/update',
  upload.single('excel'),
  check_semester,
  get_schoolYear_index,
  extract_data,
  count_droped_out_students,
  count_not_interact_students,
  count_prompted_students,
  count_subject_to_academic_warning_students,
  update_students,
  update_class
);

router.get('/template/:classname', auth, is_specialist, get_class_template);

router.get('/:classname', get_class_by_classname);

router.patch('/:classname', edit_class_info);

router.delete(
  '/:classname',
  delete_students_from_class,
  delete_parents,
  delete_students_from_user,
  delete_class
);
module.exports = router;
