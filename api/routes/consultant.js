const express = require('express')
const router = express.Router()
const multer = require('multer')

const {
  get_all_consultants,
  create_new_consultant,
  get_consultant_by_id,
  edit_consultant_info,
  delete_consultant
} = require('../controllers/consultant')

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './uploads/')
  },
  filename: function (req, file, cb) {
    cb(null, new Date().toISOString().replace(/:/g, '-') + file.originalname)
  }
})

const fileFilter = (req, file, cb) => {
  // reject a file
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
    cb(null, true)
  } else {
    cb(null, false)
  }
}

const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 5
  },
  fileFilter: fileFilter
})

router.get('/', get_all_consultants)

router.post('/', upload.single('profileImage'), create_new_consultant) //trong models consultant là profileImage thì tên cx phải là profileImage

router.get('/:consultantId', get_consultant_by_id)

router.patch('/:consultantId', edit_consultant_info)

router.delete('/:consultantId', delete_consultant)

module.exports = router
