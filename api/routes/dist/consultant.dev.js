"use strict";

var express = require('express');

var router = express.Router();

var multer = require('multer');

var _require = require('../controllers/consultant'),
    get_all_consultants = _require.get_all_consultants,
    create_new_consultant = _require.create_new_consultant,
    get_consultant_by_id = _require.get_consultant_by_id,
    edit_consultant_info = _require.edit_consultant_info,
    delete_consultant = _require.delete_consultant;

var storage = multer.diskStorage({
  destination: function destination(req, file, cb) {
    cb(null, './uploads/');
  },
  filename: function filename(req, file, cb) {
    cb(null, new Date().toISOString().replace(/:/g, '-') + file.originalname);
  }
});

var fileFilter = function fileFilter(req, file, cb) {
  // reject a file
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

var upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 5
  },
  fileFilter: fileFilter
});
router.get('/', get_all_consultants);
router.post('/', upload.single('profileImage'), create_new_consultant); //trong models consultant là profileImage thì tên cx phải là profileImage

router.get('/:consultantId', get_consultant_by_id);
router.patch('/:consultantId', edit_consultant_info);
router["delete"]('/:consultantId', delete_consultant);
module.exports = router;