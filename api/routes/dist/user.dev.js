"use strict";

var express = require('express');

var _require = require('../../helpers/multer'),
    upload = _require.upload;

var router = express.Router();

var _require2 = require('../controllers/user'),
    get_all_none_student_users = _require2.get_all_none_student_users,
    sign_up = _require2.sign_up,
    login_as_student = _require2.login_as_student,
    login_as_staff = _require2.login_as_staff,
    get_user_by_id = _require2.get_user_by_id,
    update_user = _require2.update_user,
    delete_user = _require2.delete_user,
    search = _require2.search,
    edit_current_user = _require2.edit_current_user,
    get_current_user = _require2.get_current_user,
    export_xlsx = _require2.export_xlsx,
    send_mail = _require2.send_mail,
    merge_mail = _require2.merge_mail,
    add_new_user_to_collection_User = _require2.add_new_user_to_collection_User;

var _require3 = require('../middleware/user'),
    auth = _require3.auth,
    is_specialist = _require3.is_specialist,
    send_mail_confirm = _require3.send_mail_confirm,
    upload_cloud_attach = _require3.upload_cloud_attach,
    add_new_user_to_specific_collection = _require3.add_new_user_to_specific_collection;

var _require4 = require('../middleware/consultant'),
    delete_consultant = _require4.delete_consultant,
    update_consultant = _require4.update_consultant;

router.get('/me', auth, get_current_user);
router.patch('/me', auth, edit_current_user);
router.get('/none/student/user', auth, get_all_none_student_users);
router.get('/search/:keyword', search);
router.post('/', auth, is_specialist, add_new_user_to_specific_collection, add_new_user_to_collection_User);
router.post('/signup', sign_up, send_mail_confirm);
router.post('/login/student', login_as_student);
router.post('/login/staff', login_as_staff);
router.get('/:role/:code', get_user_by_id);
router.patch('/:userCode', update_user, update_consultant);
router["delete"]('/as/consultant/:userCode', auth, is_specialist, delete_user, delete_consultant);
router["delete"]('/as/leader/:userCode', auth, is_specialist, delete_user);
router.post('/export/xlsx', auth, is_specialist, export_xlsx);
router.post('/mail', upload.single('attach'), upload_cloud_attach, send_mail);
router.post('/merge/mail', auth, merge_mail);
module.exports = router;