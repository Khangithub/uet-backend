const express = require('express');
const router = express.Router();

const {
  get_all_faculties,
  add_new_faculty,
  edit_faculty_info,
  delete_faculty,
} = require('../controllers/faculty');
const {auth} = require('../middleware/user');

router.get('/', get_all_faculties);

router.post('/', auth, add_new_faculty);

router.patch('/:code', auth, edit_faculty_info);

router.delete('/:code', auth, delete_faculty);

module.exports = router;
