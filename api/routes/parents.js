const express = require('express');
const router = express.Router();

const {auth} = require('../middleware/user');
const {get_parents, get_current_parents, edit_parents} = require('../controllers/parents');

router.get('/me', auth, get_current_parents);

router.get(
  '/:code',
  get_parents
);


router.patch('/:code', auth, edit_parents);

module.exports = router;
