var express = require('express');
var router = express.Router();

const {
  get_students_from_class,
  get_student,
  update_student_gpa,
  update_student_gpa_semester,
  update_student_fCredits,
  re_evaluate_warnings,
  update_warning_manually,
  delete_student,
} = require('../controllers/student');

const {delele_student_from_user} = require('../middleware/user');

const {delele_student_from_parents} = require('../middleware/parents');

const {
  get_schoolYear_index,
  decrease_current_classSize_and_warnedLength_by_one,
} = require('../middleware/class');

const {auth, is_specialist} = require('../middleware/user');

router.get('/from/class/:classname', get_students_from_class);

router.get('/with/code/:code', get_student);

router.patch('/:code/warning', update_warning_manually);

router.patch(
  '/:code',
  auth,
  is_specialist,
  get_schoolYear_index,
  update_student_gpa,
  update_student_gpa_semester,
  update_student_fCredits,
  re_evaluate_warnings
);

router.delete(
  '/:classname/:code',
  auth,
  is_specialist,
  delete_student,
  delele_student_from_user,
  delele_student_from_parents,
  decrease_current_classSize_and_warnedLength_by_one
);

module.exports = router;
