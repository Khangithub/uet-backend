const express = require('express');
const {upload} = require('../../helpers/multer');
const router = express.Router();

const {
  get_all_none_student_users,
  sign_up,
  login_as_student,
  login_as_staff,
  get_user_by_id,
  update_user,
  delete_user,
  search,
  edit_current_user,
  get_current_user,
  export_xlsx,
  send_mail,
  merge_mail,
  add_new_user_to_collection_User,
} = require('../controllers/user');

const {
  auth,
  is_specialist,
  send_mail_confirm,
  upload_cloud_attach,
  add_new_user_to_specific_collection,
} = require('../middleware/user');

const {delete_consultant, update_consultant} = require('../middleware/consultant');

router.get('/me', auth, get_current_user);

router.patch('/me', auth, edit_current_user);

router.get('/none/student/user', auth, get_all_none_student_users);

router.get('/search/:keyword', search);

router.post(
  '/',
  auth,
  is_specialist,
  add_new_user_to_specific_collection,
  add_new_user_to_collection_User
);

router.post('/signup', sign_up, send_mail_confirm);

router.post('/login/student', login_as_student);

router.post('/login/staff', login_as_staff);

router.get('/:role/:code', get_user_by_id);

router.patch('/:userCode', update_user, update_consultant);

router.delete(
  '/as/consultant/:userCode',
  auth,
  is_specialist,
  delete_user,
  delete_consultant
);
router.delete('/as/leader/:userCode', auth, is_specialist, delete_user);

router.post('/export/xlsx', auth, is_specialist, export_xlsx);

router.post('/mail', 
upload.single('attach'), 
upload_cloud_attach, 
send_mail);

router.post('/merge/mail', auth, merge_mail);

module.exports = router;
