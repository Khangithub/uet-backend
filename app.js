const express = require('express')
const app = express()
const morgan = require('morgan')
const cors = require('cors')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')

const consultantRoutes = require('./api/routes/consultant')
const studentRoutes = require('./api/routes/student')
const classRoutes = require('./api/routes/class')
const userRoutes = require('./api/routes/user')
const facultyRoutes = require('./api/routes/faculty')
const parentsRoutes = require('./api/routes/parents')

mongoose.connect(
  'mongodb+srv://conor_9tails:Mongoatlas123@cluster0-xcpy1.mongodb.net/uet-thesis?retryWrites=true&w=majority',
  {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useUnifiedTopology: false, // sau 30000ms mongo server sẽ tự động đóng
    useFindAndModify: false,
    useUnifiedTopology: true
  }
)

mongoose.Promise = global.Promise

mongoose.connection.on('connected', () => {
  console.log('database: uet-thesis')
})

app.use(morgan('dev'))
app.use(cors())
app.use('/uploads', express.static('uploads'))
app.use(bodyParser.json({ limit: '50mb' }))
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }))

// Routes which should handle requests
app.use('/consultants', consultantRoutes)
app.use('/students', studentRoutes)
app.use('/classes', classRoutes)
app.use('/users', userRoutes)
app.use('/faculties', facultyRoutes)
app.use('/parents', parentsRoutes)

app.use((req, res, next) => {
  const error = new Error('Not found')
  error.status = 404
  next(error)
})

app.use((error, req, res, next) => {
  res.status(error.status || 500)
  res.json({
    error: {
      message: error.message
    }
  })
})

module.exports = app
