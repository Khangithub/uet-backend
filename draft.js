[
   {
      "faculty":{
         "_id":"5f9c3e04e5a5423cec34b2e2",
         "code":"khmt",
         "facultyName":"Computer Science"
      },
      "totalClassSize":91,
      "totalWarnedLength":61
   },
   {
      "faculty":{
         "_id":"5f9c53c8a0db8f4240ec6f71",
         "code":"dtvt",
         "facultyName":"Electronics and Telecommunication"
      },
      "totalClassSize":148, // total classSize of all class has facultyName is "Electronics and Telecommunication"
      "totalWarnedLength":64 // total warnedLength of all class has facultyName is "Electronics and Telecommunication"
   },
   {
      "faculty":{
         "_id":"5f9c53c8a0db8f4240ec6de0",
         "code":"hkvt",
         "facultyName":"Space Airline"
      },
      "totalClassSize":60, // total classSize of all class has facultyName is "Space Airline"
      "totalWarnedLength":30 // total warnedLength of all class has facultyName is "Space Airline"
   },
]


[
   {
      "faculty":{
         "_id":"5f9c3e04e5a5423cec34b2e2",
         "code":"khmt",
         "facultyName":"Computer Science"
      },
      "classSize":91,
      "warnedLength":61
   },
   {
      "faculty":{
         "_id":"5f9c53c8a0db8f4240ec6f71",
         "code":"dtvt",
         "facultyName":"Electronics and Telecommunication"
      },
      "classSize":89,
      "warnedLength":44
   },
   {
      "faculty":{
         "_id":"5f9c53c8a0db8f4240ec6f71",
         "code":"dtvt",
         "facultyName":"Electronics and Telecommunication"
      },
      "classSize":59,
      "warnedLength":20
   },
   {
      "faculty":{
         "_id":"5f9c53c8a0db8f4240ec6f71",
         "code":"hkvt",
         "facultyName":"Space Airline"
      },
      "classSize":10,
      "warnedLength":5
   },
   {
      "faculty":{
         "_id":"5f9c53c8a0db8f4240ec6f71",
         "code":"hkvt",
         "facultyName":"Space Airline"
      },
      "classSize":20,
      "warnedLength":10
   },
   {
      "faculty":{
         "_id":"5f9c53c8a0db8f4240ec6f71",
         "code":"hkvt",
         "facultyName":"Space Airline"
      },
      "classSize":30,
      "warnedLength":15
   }
]