const XLSX = require('xlsx')
const config = require('./config')
var admin = require('firebase-admin')
const uuid = require('uuid-v4')

// CHANGE: The path to your service account
var serviceAccount = require('./serviceAccountKey.json')

exports.uploadExcel = async (data, filename) => {
  var wb = XLSX.utils.book_new() // create new workbook
  var sheet = XLSX.utils.json_to_sheet(data) // convert json to worksheet

  XLSX.utils.book_append_sheet(wb, sheet, 'Sheet1') // add worksheet to workbook
  XLSX.writeFile(wb, `./uploads/${filename}.xlsx`) // write file

  if (!admin.apps.length) {
    backendApp = admin.initializeApp({
      credential: admin.credential.cert(serviceAccount),
      storageBucket: config.storageBucket
    })
  }

  var path = `uploads/${filename}.xlsx`

  const metadata = {
    metadata: {
      firebaseStorageDownloadTokens: uuid()
    },
    cacheControl: 'public, max-age=31536000'
  }

  await backendApp
    .storage()
    .bucket()
    .upload(path, {
      gzip: true,
      metadata: metadata
    })

  return `https://firebasestorage.googleapis.com/v0/b/${config.storageBucket}/o/${filename}.xlsx?alt=media&token=${metadata.metadata.firebaseStorageDownloadTokens}`
}

exports.uploadFile = async (file) => {
  if (!admin.apps.length) {
    backendApp = admin.initializeApp({
      credential: admin.credential.cert(serviceAccount),
      storageBucket: config.storageBucket
    })
  }

  const metadata = {
    metadata: {
      firebaseStorageDownloadTokens: uuid()
    },
    cacheControl: 'public, max-age=31536000'
  }

  await backendApp
    .storage()
    .bucket()
    .upload(file.path, {
      gzip: true,
      metadata: metadata
    })

  // https://firebasestorage.googleapis.com/v0/b/fir-fb-auth.appspot.com/o/attach-1602949177137-avatar.PNG?alt=media&token=7093633c-a0da-4737-af18-fe4aeb970f9b
  return `https://firebasestorage.googleapis.com/v0/b/${config.storageBucket}/o/${file.filename}?alt=media&token=${metadata.metadata.firebaseStorageDownloadTokens}`
}