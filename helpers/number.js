const randomInt = (min, max) => {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

exports.generateFCreditsBaseOnGPA = (score) => {
  var {gpa} = score;

  return (
    (gpa >= 3.5 && randomInt(0, 5)) ||
    (gpa >= 3.0 && gpa < 3.5 && randomInt(5, 10)) ||
    (gpa >= 2.5 && gpa < 3.0 && randomInt(10, 15)) ||
    (gpa >= 2.0 && gpa < 2.5 && randomInt(15, 20)) ||
    (gpa >= 1.5 && gpa < 2.0 && randomInt(20, 25)) ||
    (gpa < 1.5 && randomInt(25, 30))
  );
};

exports.randomFloat = (min, max) => {
  return parseFloat((Math.random() * (max - min) + min).toFixed(2));
};

exports.classifyGPA = (gpa) => {
  var result =
    (gpa >= 3.5 && gpa <= 4 && 'Aplus') ||
    (gpa >= 3.0 && gpa < 3.5 && 'Ascore') ||
    (gpa >= 2.5 && gpa < 3.0 && 'Bplus') ||
    (gpa >= 2.0 && gpa < 2.5 && 'Bscore') ||
    (gpa >= 1 && gpa < 2.0 && 'Cscore') ||
    (gpa < 1 && 'Fscore');

  console.log(result, gpa);

  return result;
};

exports.isNum = (str) => {
  var isNum = /^\d+$/.test(str.replace(/\s/g, 'X'));
  return isNum;
};
