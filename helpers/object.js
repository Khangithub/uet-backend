exports.classifyAcademicWarningForFreshman = (
  schoolYear,
  gpaSemester,
  gpa,
  shortenCredits
) => {
  var warning = {
    schoolYear,
    semester: '1',
    reason: [],
  };
  if (gpaSemester < 0.8) {
    warning.reason.push(
      `GPA at the first semester of ${schoolYear} less than 0.8 (${gpaSemester})`
    );
  }

  if (shortenCredits > 24) {
    warning.reason.push(
      `The number of credits F is greater than 24 (${shortenCredits})`
    );
  }

  if (gpa < 1.2) {
    warning.reason.push(
      `Freshman with a cumulative GPA of less than 1.2 (${gpa})`
    );
  }

  return warning;
};

exports.updateAcademicWarning = (
  schoolYearIndex,
  schoolYear,
  semester,
  gpaSemester,
  gpa,
  shortenCredits
) => {

  var warning = {
    schoolYear,
    semester,
    reason: [],
  };

  // Freshman
  if (schoolYearIndex === 0) {
    if (gpaSemester < 1) {
      warning.reason.push(
        `GPA at the ${
          semester === '1' ? 'first' : 'second'
        } semester of ${schoolYear} less than 1.0 (${gpaSemester})`
      );
    }

    if (shortenCredits > 24) {
      warning.reason.push(
        `The number of credits F is greater than 24 (${shortenCredits})`
      );
    }

    if (gpa < 1.2) {
      warning.reason.push(
        `Freshman with a cumulative GPA of less than 1.2 (${gpa})`
      );
    }
  }

  // Sophomore
  if (schoolYearIndex === 1) {
    if (gpaSemester < 1) {
      warning.reason.push(
        `GPA at the ${
          semester === '1' ? 'first' : 'second'
        } semester of ${schoolYear} less than 1.0 (${gpaSemester})`
      );
    }

    if (shortenCredits > 24) {
      warning.reason.push(
        `The number of credits F is greater than 24 (${shortenCredits})`
      );
    }

    if (gpa < 1.4) {
      warning.reason.push(
        `Sophomore with a cumulative GPA of less than 1.4 (${gpa})`
      );
    }
  }

  // third-year student
  if (schoolYearIndex === 2) {
    if (gpaSemester < 1) {
      warning.reason.push(
        `GPA at the ${
          semester === '1' ? 'first' : 'second'
        } semester of ${schoolYear} less than 1.0 (${gpaSemester})`
      );
    }

    if (shortenCredits > 24) {
      warning.reason.push(
        `The number of credits F is greater than 24 (${shortenCredits})`
      );
    }

    if (gpa < 1.6) {
      warning.reason.push(
        `Third-year student with a cumulative GPA of less than 1.6 (${gpa})`
      );
    }
  }

  // final-year student
  if (schoolYearIndex >= 3) {
    if (gpaSemester < 1) {
      warning.reason.push(
        `GPA at the ${
          semester === '1' ? 'first' : 'second'
        } semester of ${schoolYear} less than 1.0 (${gpaSemester})`
      );
    }

    if (shortenCredits > 24) {
      warning.reason.push(
        `The number of credits F is greater than 24 (${shortenCredits})`
      );
    }

    if (gpa < 1.8) {
      warning.reason.push(
        `Final-year student with a cumulative GPA of less than 1.8 (${gpa})`
      );
    }
  }
  return warning;
};
