exports.formatClassname = classname => {
  return classname.replace(/\W/gi, '').toLowerCase()
}